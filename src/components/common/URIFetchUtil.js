import React from 'react'

var createReactClass = require('create-react-class');

const URIFetchUtil = createReactClass({
    statics: {
      extractHash: function(url, dataToReplace) {
        let DATA_TO_BE_REPLACED = dataToReplace;
        return url.replace(DATA_TO_BE_REPLACED, "");
      },
    },
    render() {
    },
  });
  
  export default URIFetchUtil;