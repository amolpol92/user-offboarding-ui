/* eslint-disable react/no-unescaped-entities */
import React from "react";
import "../styles/NotFound.scss";

export default () =>
  <div className="NotFound">
    <h3>This page is currently unavailable</h3>
  </div>;

