import React, { Component } from 'react'
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import OptOutComponent from './components/OptOut/OptOutComponent'
import UnsubscribeComponent from './components/unsubscribe/UnsubscribeComponent';
import UpdatePreferences from './components/update-preferences/UpdatePreferences';
import ApplicationUnsubscribeComponent from './components/application-unsubcribe/ApplicationUnsubscribeComponent';
//import OPT_OUT_URL from './constants/URLConstants/OPT_OUT_URL';

export default () => {
  return (
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/api/users/opt-out/:hash" component={OptOutComponent} exact />
      <Route path="/api/users/unsubscribe-group/:hash" component={UnsubscribeComponent} exact />
      <Route path="/api/users/unsubscribe-application/:hash" component={ApplicationUnsubscribeComponent} exact />
      <Route path="/api/users/manage-subscriptions/:hash" component={UpdatePreferences} exact />
      <Route exact component={NotFound} />
       <Redirect from='*' to='/' />
    </Switch>
  );
}
