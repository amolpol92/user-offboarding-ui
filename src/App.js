import React, { Component } from 'react'
import Routes from "./Routes";
import Routers from './components/RouteComponent/Routers';
import { BrowserRouter, HashRouter } from 'react-router-dom';

export class App extends Component {
  render() {
    return (
      <div>
        <Routers />
      </div>
    );
  }
}


export default App
